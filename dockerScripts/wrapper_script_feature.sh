#!/bin/bash

# turn on bash's job control
set -m

# Start the primary process and put it in the background: main_app
echo 'init main process'
sh ./dockerScripts/feature/main_app_process_feature.sh &

# Start the process 2 : map_app
echo 'init process 2'
sh ./dockerScripts/feature/map_app_process_2.sh &

echo 'init process 3'
sh ./dockerScripts/feature/table_app_process_3.sh &

echo 'init process 4'
sh ./dockerScripts/feature/web_comp_process_4.sh

# the my_helper_process might need to know how to wait on the
# primary process to start before it does its work and returns


# now we bring the primary process back into the foreground
# and leave it there
echo 'switch to main process'
fg %1