/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  Req,
  Res,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { AppService, ExternalGridFailure } from './app.service';
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('/internal-sit')
  createPublicSITs(@Req() req: Request, @Res() res: Response): void {
    try {
      const body: ExternalGridFailure = req.body;
      if (!process.env.PASSWORD) {
        console.error('Web-Cache Password existiert nicht.');
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
      } else if (!body.password) {
        console.error('Backend Password existiert nicht.');
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
      } else if (process.env.PASSWORD !== body.password) {
        console.error('Password stimmt nicht überein.');
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
      }

      this.appService.saveGridFailures(
        body,
        (isSuccesFullSave: boolean, message: string) => {
          res.statusMessage = message;
          isSuccesFullSave && res.sendStatus(HttpStatus.CREATED);
        },
      );
    } catch (error) {
      res.sendStatus(error.status);
    }
  }

  @Post('/fe-settings')
  createPublicSettings(@Req() req: Request, @Res() res: Response): void {
    try {
      this.appService.saveSettings(
        req.body,
        (isSuccesFullSave: boolean, message: string) => {
          res.statusMessage = message;
          isSuccesFullSave && res.sendStatus(HttpStatus.CREATED);
        },
      );
    } catch (error) {
      res.sendStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('/public-sit')
  getSITs(@Res() res: Response) {
    try {
      this.appService.getSITs((data: any) => {
        if (!!data && !!data.length) {
          res.status(HttpStatus.OK);
          res.send(data);
        } else {
          res.statusMessage = 'File is empty of file not found!';
          res.sendStatus(HttpStatus.NOT_FOUND);
        }
      });
    } catch (error) {
      res.sendStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('/public-settings')
  getSettings(@Res() res: Response) {
    try {
      this.appService.getSettings((data: any) => {
        if (!!data && !!data.length) {
          res.status(HttpStatus.OK);
          res.send(data);
        } else {
          res.statusMessage = 'File is empty or file not found!';
          res.status(HttpStatus.NOT_FOUND);
          res.send(data);
        }
      });
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR);
      res.send([]);
    }
  }
}
