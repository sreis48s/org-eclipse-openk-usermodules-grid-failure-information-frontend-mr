/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { HttpService, Query, GET, Path, Adapter, PUT, Body, DefaultHeaders } from '@grid-failure-information-app/shared/async-services/http';
import { Observable, of } from 'rxjs';
import { Settings } from '@grid-failure-information-app/shared/models/settings.model';
import { ConfigService } from '@grid-failure-information-app/app/app-config.service';
import { InitialEmailContent } from '@grid-failure-information-app/shared/models/settings-initial-email-content.model';

@Injectable()
@DefaultHeaders({
  Accept: 'application/json',
  'Content-Type': 'application/json',
})
export class AppConfigApiClient extends HttpService {

  @GET('/settings')
  @Adapter(ConfigService.configAdapter)
  public getPreConfiguration(): Observable<Settings> {
    return null;
  }

  @GET('/settings/initial-email-content')
  @Adapter(ConfigService.initialEmailContentAdapter)
  public getInitialEmailContent(): Observable<InitialEmailContent> {
    return null;
  }

}
