/********************************************************************************
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { AfterViewChecked, Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { DistributionGroupSandbox } from '../distribution-group.sandbox';

@Component({
  selector: 'app-distribution-group-allowed',
  templateUrl: './distribution-group-allowed.component.html',
  styleUrls: ['./distribution-group-allowed.component.scss'],
})
export class DistributionGroupAllowedComponent implements AfterViewChecked, OnInit {
  @Input()
  allowedGroups = null;

  internGroups = [];

  form: FormArray = new FormArray([]);

  constructor(public sandbox: DistributionGroupSandbox) {}

  ngOnInit(): void {
    this.setFormState();
  }

  setFormState() {
    const rows: FormGroup[] = [];

    for (let i = 0; i < this.internGroups.length; i++) {
      const row = {};
      row['id'] = new FormControl(i);
      row['classificationId'] = new FormControl(this.internGroups[i].classificationId);
      row['branchId'] = new FormControl(this.internGroups[i].branchId);
      row['autoSet'] = new FormControl(this.internGroups[i].autoSet);
      row['deleted'] = new FormControl(this.internGroups[i].deleted);

      rows.push(new FormGroup(row));
    }

    this.form = new FormArray(rows);
  }

  addRow() {
    this.internGroups.push({
      classificationId: '',
      branchId: '',
      deleted: false,
    });
    this.setFormState();
  }

  updateElem(rowId: number) {
    this.allowedGroups[rowId].branchId = this.form.controls[rowId].get('branchId').value;
    this.allowedGroups[rowId].classificationId = this.form.controls[rowId].get('classificationId').value;
    this.allowedGroups[rowId].autoSet = this.form.controls[rowId].get('autoSet').value;
  }

  delete(rowId: number) {
    if (this.allowedGroups[rowId].id) {
      this.allowedGroups[rowId].deleted = !this.allowedGroups[rowId].deleted;
    } else {
      this.allowedGroups.splice(rowId, 1);
    }
    this.setFormState();
  }

  ngAfterViewChecked() {
    if (this.allowedGroups && this.allowedGroups !== this.internGroups) {
      setTimeout(() => {
        this.internGroups = this.allowedGroups;
        this.setFormState();
      });
    }
  }
}
