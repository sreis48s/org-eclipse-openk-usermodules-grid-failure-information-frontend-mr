/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ModeEnum } from '@grid-failure-information-app/shared/constants/enums';
import { of } from 'rxjs';
import { HeaderCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/header-cell-renderer/header-cell-renderer.component';

describe('HeaderCellRendererComponent', () => {
  let component: HeaderCellRendererComponent;

  beforeEach(() => {
    component = new HeaderCellRendererComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init the renderer and define the addHeaderIcon if there is an context icon', () => {
    const params: any = {
      context: {
        icons: { add: 'im_an_icon' },
        eventSubject: {
          subscribe() {},
        },
      },
    };
    component.agInit(params);
    expect(component.addHeaderIcon).toBeDefined();
    expect(component.addHeaderIcon).toBe(true);
  });

  it('should call updateIcon and hide addHeaderIcon if mode is InitialMode and there are no context icons', () => {
    const event: any = { eventType: ModeEnum.InitialMode };
    const params: any = {
      context: {
        eventSubject: of(event),
      },
    };
    component.agInit(params);

    expect(component.addHeaderIcon).toBeFalsy();
  });

  it('should call updateIcon and show addHeaderIcon if mode is OverviewTableSelectionMode and there are no context icons', () => {
    const event: any = { eventType: ModeEnum.OverviewTableSelectionMode };
    const params: any = {
      context: {
        eventSubject: of(event),
      },
    };
    component.agInit(params);

    expect(component.addHeaderIcon).toBeTruthy();
  });

  it('should call updateIcon and dont set addHeaderIcon if mode is anything else and there are no context icons', () => {
    const dontChangedAddIconValue: boolean = true;
    component.addHeaderIcon = dontChangedAddIconValue;
    const event: any = { eventType: 'im_not_initial_and_not_overview_mode' };
    const params: any = {
      context: {
        eventSubject: of(event),
      },
    };
    component.agInit(params);

    expect(component.addHeaderIcon).toBe(dontChangedAddIconValue);
  });

  it('should transfer the updated values on click ', () => {
    const eventType: string = 'im_an_event_type';
    (component as any)._params = {
      context: {
        eventSubject: { next: function () {} },
      },
      data: 'data',
    };
    const spy = spyOn((component as any)._params.context.eventSubject, 'next');
    component.clicked(eventType);

    expect(spy).toHaveBeenCalledWith({ type: eventType, data: 'data' });
  });
});
