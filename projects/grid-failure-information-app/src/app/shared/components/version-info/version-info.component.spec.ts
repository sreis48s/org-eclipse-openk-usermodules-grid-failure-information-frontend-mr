/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { VersionInfo } from '@grid-failure-information-app/shared/components/version-info/version-info.component';

describe('VersionInfo', () => {
  let component: VersionInfo;

  beforeEach(async(() => {}));

  beforeEach(() => {
    component = new VersionInfo();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call back', () => {
    const currVersion: any = {
      frontendVersion: '1.0',
      backendVersion: '1.0',
      dbVersion: '1.0',
    };
    (component as any).setVersionInfo(currVersion);
    expect(component.currVersion).toBe(currVersion);
  });
});
