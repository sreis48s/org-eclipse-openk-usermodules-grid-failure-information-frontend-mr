/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { BaseComponent } from '@grid-failure-information-app/shared/components/base-components/base.component';
import { BaseSandbox } from '@grid-failure-information-app/shared/sandbox/base.sandbox';

describe('BaseComponent', () => {
  let component: BaseComponent;
  let sandbox: BaseSandbox;

  beforeEach(() => {
    sandbox = { endSubscriptions() {} } as any;
    component = new BaseComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
