/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { SetFilterComponent } from '@grid-failure-information-app/shared/filters/ag-grid/set-filter/set-filter.component';
import { IFilterParams, RowNode } from 'ag-grid-community';
import { ALL_NGRX_FORMS_ACTION_TYPES } from 'ngrx-forms';
import { of } from 'rxjs';

describe('SetFilterComponent', () => {
  let component: SetFilterComponent;

  beforeEach(() => {
    const params: any = {
      node: 'x',
      colDef: {
        colId: 'testColId',
      },
      rowModel: {
        nodeManager: {
          allNodesMap: {
            0: { id: '0' },
            1: { id: '1' },
          },
        },
      },
      doesRowPassOtherFilter() {},
      filterChangedCallback() {},
      api: {
        getModel() {
          return {
            rowsToDisplay: {
              map: () => {
                return ['Störung'];
              },
            },
          };
        },
        gridOptionsWrapper: { gridOptions: { onGridSizeChanged: () => {}, onRowDataChanged: () => {} } },
      },
    } as any;
    component = new SetFilterComponent();
    component['_params'] = params;
    component.setItems = {
      FilterText1: { checked: true },
      FilterText2: { checkde: false },
    };
    component['_valueGetter'] = () => {
      return { id: '0' };
    };
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should extend the setItems in _extendFilterItem when filterWord is test', () => {
    const filterItems = { test: { checked: true } };
    (component as any)._filteredItems = filterItems;
    (component as any)._extendFilteredItem('test');
    expect('test' in component.setItems).toBeTruthy();
  });

  it('should filterCheckboxList with setItems', () => {
    component.setItems = [
      { AAA: {}, checked: true },
      { BBB: {}, checked: true },
    ];
    const compAnonym = component as any;
    compAnonym._params = { filterChangedCallback() {} };
    component.filterCheckboxList('X');
    const filteredItem = compAnonym._filteredItems[0];
    expect(filteredItem).toEqual({ AAA: {}, checked: true });
  });

  it('should set selectAllChecked false is an non-empty filter text is provided', () => {
    component.setItems = [
      { AAA: {}, checked: true },
      { BBB: {}, checked: true },
    ];
    const compAnonym = component as any;
    compAnonym._params = { filterChangedCallback() {} };
    component.filterText = 'Test';
    component.filterCheckboxList('X');
    expect(component.selectAllChecked).toBeFalsy();
  });

  it('should add filtered filterItem to the filterModel in _addItemToFilterModel()', () => {
    const compAnonym = component as any;
    compAnonym.filterText = 'Filter';
    compAnonym._addItemToFilterModel({}, 'FilterText');
    expect(compAnonym.setItems).toBeDefined();
    expect(compAnonym.setItems['FilterText']).toBeDefined();
  });

  it('should add a checked filtered filterItem to the filterModel in _addItemToFilterModel()', () => {
    const oldFilterItems = {};
    const compAnonym = component as any;
    compAnonym.filterText = 'Filter';
    compAnonym._addItemToFilterModel(oldFilterItems, 'FilterText');
    expect(compAnonym.setItems).toBeDefined();
    expect(compAnonym.setItems['FilterText']).toBeDefined();
    expect(compAnonym.setItems['FilterText']['checked']).toBeTruthy();
  });

  it('should add a checked filterItem to the filterModel in _addItemToFilterModel()', () => {
    const oldFilterItems = {
      FilterText: {
        checked: true,
      },
    };
    const compAnonym = component as any;
    compAnonym.filterText = '';
    compAnonym._addItemToFilterModel(oldFilterItems, 'FilterText');
    expect(compAnonym.setItems).toBeDefined();
    expect(compAnonym.setItems['FilterText']).toBeDefined();
    expect(compAnonym.setItems['FilterText']['checked']).toBeTruthy();
  });

  it('should setItems in response to filteredItems', () => {
    component.setItems = [{ BBB: {}, checked: false }];
    component['_filteredItem'] = [{ BBB: {}, checked: true }];
    const compAnonym = component as any;
    compAnonym._params = { filterChangedCallback() {} };
    component.filterCheckboxList('B');
    expect(component.setItems['BBB']).toEqual(component['_filteredItem']['BBB']);
  });

  it('should return isFilterActive', () => {
    component.setItems = [{ checked: true }, { checked: true }];
    expect(component.isFilterActive()).toBeFalsy();
  });

  it('should return model', () => {
    component.setItems = 666;
    expect(component.getModel().values).toBe(666);
  });

  it('should set model 1', () => {
    component.setItems = 666;
    component.setModel(null);
    expect(component.setItems).toEqual([]);
  });

  it('should set model 2', () => {
    component.setItems = 666;
    component.setModel({ values: 333 });
    expect(component.setItems).toEqual(333);
  });

  it('should set checked of setItems via notifyFilter (checked true)', () => {
    component['_params'] = { filterChangedCallback() {} } as any;
    component.setItems = { null: { checked: false } };
    const event = { target: { id: 'null', checked: true } } as any;
    component.notifyFilter(event);
    expect(component.setItems.null.checked).toBeTruthy();
  });

  it('should set checked of setItems via notifyFilter (checked false)', () => {
    component['_params'] = { filterChangedCallback() {} } as any;
    component.setItems = { null: { checked: false } };
    const event = { target: { id: 'null', checked: false } } as any;
    component.notifyFilter(event);
    expect(component.setItems.null.checked).toBeFalsy();
  });

  it('should set selectAllChecked false if filterText is empty', () => {
    component['_params'] = { filterChangedCallback() {} } as any;
    component.setItems = { null: { checked: false } };
    component.filterText = '';
    const event = { target: { id: 'null', checked: true } } as any;
    component.notifyFilter(event);
    expect(component.selectAllChecked).toBeFalsy();
  });

  it('should selectAll via selectAll with given event', () => {
    component['_params'] = { filterChangedCallback() {} } as any;
    component.setItems = { null: { checked: false } };
    const event = { target: { id: 'null', checked: true } } as any;
    component.selectAll(event);
    expect(component.selectAllChecked).toBeTruthy();
  });

  it('should selectAll via selectAll without a given event', () => {
    component['_params'] = { filterChangedCallback() {} } as any;
    component.setItems = { null: { checked: false } };
    component.selectAll();
    expect(component.selectAllChecked).toBeTruthy();
  });

  it('should not pass filter for undefined setItems', () => {
    const params = { node: 'x' } as any;
    component['_valueGetter'] = () => {};
    const returnValue = component.doesFilterPass(params);
    expect(returnValue).toBeFalsy();
  });

  it('should not pass filter for itemKey null', () => {
    const params = { node: '' } as any;
    component['_valueGetter'] = () => params.node;
    component.setItems = { null: { checked: false } };
    const returnValue = component.doesFilterPass(params);
    expect(returnValue).toBeFalsy();
  });

  it('should return true after calling doesFilterPass(..) with filter 50 and node value 50 ', () => {
    component['_valueGetter'] = () => 50;
    component.setItems = { 50: { checked: true } };
    const returnValue = component.doesFilterPass({} as any);
    expect(returnValue).toBeTruthy();
  });

  it('should return false after calling doesFilterPass(..) with filter 50 and node value 666 ', () => {
    component['_valueGetter'] = () => 666;
    component.setItems = { 50: { checked: true } };
    const returnValue = component.doesFilterPass({} as any);
    expect(returnValue).toBeFalsy();
  });

  it('should return false after calling doesFilterPass(..) with filter "oncle" and node value "tom" ', () => {
    component['_valueGetter'] = () => 'tom';
    component.setItems = { oncle: { checked: true } };
    const returnValue = component.doesFilterPass({} as any);
    expect(returnValue).toBeFalsy();
  });

  it('should return true after calling doesFilterPass(..) with filter "oncle" and node value "oncle" ', () => {
    component['_valueGetter'] = () => 'oncle';
    component.setItems = { oncle: { checked: true } };
    const returnValue = component.doesFilterPass({} as any);
    expect(returnValue).toBeTruthy();
  });

  it('should return true after calling doesFilterPass(..) with filter "oncle" and node value "oncle tom" ', () => {
    component['_valueGetter'] = () => 'oncle tom';
    component.setItems = { oncle: { checked: true } };
    const returnValue = component.doesFilterPass({} as any);
    expect(returnValue).toBeFalsy();
  });

  it('should set params via agInit()', () => {
    const params: any = {
      node: 'x',
      colDef: {
        colId: 'testColId',
      },
      rowModel: {
        nodeManager: {
          allNodesMap: {
            0: { id: '0' },
            1: { id: '1' },
          },
        },
      },
      doesRowPassOtherFilter() {},
      filterChangedCallback() {},
      api: {
        getModel() {
          return {
            rowsToDisplay: {
              map: () => {
                return ['Störung'];
              },
            },
          };
        },
        gridOptionsWrapper: {
          gridOptions: {
            onGridSizeChanged: () => {},
            onRowDataChanged: () => {},
          },
        },
      },
    } as any;
    component['_valueGetter'] = () => {
      return { id: '0' };
    };
    component.agInit(params);
    expect(component['_params']).toEqual(params as any);
  });

  it('should setFilterItems via afterGuiAttached', () => {
    const spy = spyOn(component as any, '_setFilterItems');
    component.afterGuiAttached('x');
    expect(spy).toHaveBeenCalled();
  });

  it('should return false in _areAllFilterItemsSelected() when filter textbox is not empty', () => {
    component.filterText = '0815';
    const allFilterItemsSelected: boolean = (component as any)._areAllFilterItemsSelected();
    expect(allFilterItemsSelected).toBe(false);
  });
});
