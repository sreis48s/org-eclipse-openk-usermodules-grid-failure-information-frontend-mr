/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { VisibilityEnum } from '@grid-failure-information-app/shared/constants/enums';
import { GridFailure } from '@grid-failure-information-app/shared/models';
import { AppConfigService } from '@grid-failure-information-map-app/app/app-config.service';
import { GridFailureService } from '@grid-failure-information-map-app/app/grid-failure/grid-failure.service';
import { MapOptions } from '@openk-libs/grid-failure-information-map/shared/models/map-options.model';
import { combineLatest, EMPTY, Subject, Subscription } from 'rxjs';
import { catchError, take } from 'rxjs/operators';

@Injectable()
export class GridFailureSandbox {
  public gridFailureMapList$ = new Subject<GridFailure[]>();
  public mapOptions$ = new Subject();

  private _gridFailureMapListAll: GridFailure[] = [];
  private _gridFailureMapListAllConfigured: GridFailure[] = [];
  private _subscription: Subscription = new Subscription();

  constructor(private _gridFailureService: GridFailureService, private _configService: AppConfigService) {}

  public initSandbox() {
    this._subscription = combineLatest([
      this._configService.getConfig().pipe(
        catchError(error => {
          console.error('Error fetching configuration:', error);
          const mapOptions = new MapOptions();
          mapOptions.extendMarkerInformation = true;
          this.mapOptions$.next(mapOptions);
          return EMPTY;
        })
      ),
      this._gridFailureService.getGridFailureData().pipe(
        catchError(error => {
          console.error('Error fetching grid failure data:', error);
          return EMPTY;
        })
      )
    ])
      .pipe(take(1))
      .subscribe(([config, data]: [MapOptions, GridFailure[]]) => {
        const mapOptions = new MapOptions(config);
        mapOptions.extendMarkerInformation = true;
        this.mapOptions$.next(mapOptions);

        this.gridFailureMapList$.next(
          config && config.dataExternInitialVisibility === VisibilityEnum.HIDE ? [] : data
        );

        this._gridFailureMapListAll = data;
        this._gridFailureMapListAllConfigured = config && config.dataExternInitialVisibility === VisibilityEnum.HIDE ? [] : data;
      });
  }


  public filterGridFailureMapList(postcode: string = '') {
    postcode = postcode.trim();
    if (postcode.length > 0) {
      const filteredData = this._gridFailureMapListAll.filter(
        y => y.postcode === postcode || y.freetextPostcode === postcode
      );
      this.gridFailureMapList$.next(filteredData);
    } else {
      this.gridFailureMapList$.next(this._gridFailureMapListAllConfigured);
    }
  }

  public unsubscribe() {
    this._subscription.unsubscribe();
  }
}
